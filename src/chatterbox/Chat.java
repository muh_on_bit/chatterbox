package chatterbox;

import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;

/**
 *
 * @author muzammil
 */
@SuppressWarnings("serial")
public class Chat extends javax.swing.JFrame {

    public static MulticastSocket mSocket;
    public static final int PORT1 = 4446;
    public static final int PORT2 = 5000;
    public static InetAddress GROUP1;
    public static InetAddress GROUP2;
    public static DatagramSocket dSocket;

    public Chat() throws IOException {
        initComponents();
    }

    private void initComponents() throws IOException {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ChatterBOX");
        setIconImage((ImageIO.read(Login.class.getResourceAsStream("/img/logo.png"))));
        setBackground(new java.awt.Color(39, 39, 39));
        setForeground(new java.awt.Color(39, 39, 39));
        setMaximumSize(new java.awt.Dimension(800, 630));
        setMinimumSize(new java.awt.Dimension(800, 630));
        setResizable(false);
        setSize(new java.awt.Dimension(800, 630));
        getContentPane().setLayout(null);

        jPanel2.setBackground(new java.awt.Color(39, 39, 39));

        jScrollPane4.setBorder(javax.swing.BorderFactory.createCompoundBorder(
                new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 0, true),
                new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 0, true)));
        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane4.setViewportBorder(javax.swing.BorderFactory.createCompoundBorder(
                new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 0, true), null));
        jScrollPane4.setFont(new java.awt.Font("Montserrat", 0, 14)); // NOI18N

        jTextArea1.setEditable(false);
        jTextArea1.setText("");
        jTextArea1.setBackground(new java.awt.Color(48, 48, 48));
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Montserrat", 1, 14)); // NOI18N
        jTextArea1.setForeground(new java.awt.Color(224, 224, 224));
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 15));
        jTextArea1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTextArea1.setMargin(new java.awt.Insets(15, 15, 0, 15));
        jScrollPane4.setViewportView(jTextArea1);

        jScrollPane1.setBorder(null);
        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextArea2.setEditable(false);
        jTextArea2.setBackground(new java.awt.Color(32, 32, 32));
        jTextArea2.setText("");
        jTextArea2.setColumns(20);
        jTextArea2.setFont(new java.awt.Font("Montserrat", 1, 18)); // NOI18N
        jTextArea2.setForeground(new java.awt.Color(224, 224, 224));
        jTextArea2.setLineWrap(true);
        jTextArea2.setRows(5);
        jTextArea2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTextArea2.setMargin(new java.awt.Insets(15, 15, 0, 15));
        jScrollPane1.setViewportView(jTextArea2);

        jLabel1.setBackground(new java.awt.Color(48, 48, 48));
        jLabel1.setFont(new java.awt.Font("Montserrat SemiBold", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(186, 186, 186));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Chat Area");
        jLabel1.setOpaque(true);

        jLabel2.setBackground(new java.awt.Color(32, 32, 32));
        jLabel2.setFont(new java.awt.Font("Montserrat SemiBold", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(186, 186, 186));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Active");
        jLabel2.setOpaque(true);

        final javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0,
                        100, Short.MAX_VALUE));
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0,
                        100, Short.MAX_VALUE));

        jScrollPane2.setBorder(null);
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextArea3.setBackground(new java.awt.Color(158, 158, 158));
        jTextArea3.setColumns(20);
        jTextArea3.setText("");
        jTextArea3.setFont(new java.awt.Font("Montserrat", 0, 14)); // NOI18N
        jTextArea3.setForeground(new java.awt.Color(17, 17, 17));
        jTextArea3.setLineWrap(true);
        jTextArea3.setRows(4);
        jTextArea3.setCaretColor(new java.awt.Color(17, 17, 17));
        jTextArea3.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jTextArea3.setMargin(new java.awt.Insets(6, 6, 0, 6));
        jTextArea3.setMinimumSize(new java.awt.Dimension(220, 38));
        jTextArea3.setPreferredSize(new java.awt.Dimension(312, 38));
        jScrollPane2.setViewportView(jTextArea3);

        jPanel3.setBackground(new java.awt.Color(39, 39, 39));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setIcon(new javax.swing.ImageIcon((ImageIO.read(Login.class.getResourceAsStream("/img/logo.png"))))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Montserrat", 1, 48)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("ChatterBOX");

        final javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(jPanel3Layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup().addGap(131, 131, 131)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 128,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
                                Short.MAX_VALUE)));
        jPanel3Layout.setVerticalGroup(jPanel3Layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup().addGap(0, 0, 0).addGroup(jPanel3Layout
                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                Short.MAX_VALUE))));

        jPanel4.setBackground(new java.awt.Color(39, 39, 39));

        jButton1.setBackground(new java.awt.Color(239, 154, 154));
        jButton1.setFont(new java.awt.Font("Montserrat", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(33, 33, 33));
        jButton1.setText("LOGOUT");
        jButton1.setBorder(null);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener((final java.awt.event.ActionEvent evt) -> {
            try {
                jButton1ActionPerformed(evt);
            } catch (final HeadlessException e) {
            } catch (final IOException e) {
            }
        });

        final javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(jPanel4Layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout
                        .createSequentialGroup().addContainerGap(21, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 140,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)));
        jPanel4Layout.setVerticalGroup(jPanel4Layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout
                        .createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 38,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)));

        jButton2.setBackground(new java.awt.Color(39, 39, 39));
        jButton2.setForeground(new java.awt.Color(39, 39, 39));
        jButton2.setIcon(new javax.swing.ImageIcon((ImageIO.read(Login.class.getResourceAsStream("/img/send.png"))))); // NOI18N
        jButton2.setAlignmentY(0.0F);
        jButton2.setBorder(null);
        jButton2.setBorderPainted(false);
        jButton2.setContentAreaFilled(false);
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.setFocusable(false);
        jButton2.setOpaque(false);
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(final java.awt.event.MouseEvent evt) {
                jButton2MousePressed(evt);
            }

            public void mouseReleased(final java.awt.event.MouseEvent evt) {
                jButton2MouseReleased(evt);
            }
        });
        jButton2.addActionListener((final java.awt.event.ActionEvent evt) -> {
            jButton2ActionPerformed(evt);
        });

        final javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout
                        .createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(
                                javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addComponent(jPanel3,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout
                                                .createParallelGroup(
                                                        javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanel2Layout
                                                        .createSequentialGroup()
                                                        .addGap(80, 80, 80)
                                                        .addComponent(jScrollPane2,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                390,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(
                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jButton2,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                50,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel2Layout
                                                        .createSequentialGroup()
                                                        .addGap(50, 50, 50)
                                                        .addGroup(jPanel2Layout
                                                                .createParallelGroup(
                                                                        javax.swing.GroupLayout.Alignment.LEADING,
                                                                        false)
                                                                .addComponent(jLabel1,
                                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                        Short.MAX_VALUE)
                                                                .addComponent(jScrollPane4,
                                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                        500,
                                                                        Short.MAX_VALUE))))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                18, Short.MAX_VALUE)
                                        .addGroup(jPanel2Layout
                                                .createParallelGroup(
                                                        javax.swing.GroupLayout.Alignment.TRAILING,
                                                        false)
                                                .addComponent(jLabel2,
                                                        javax.swing.GroupLayout.Alignment.LEADING,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE)
                                                .addComponent(jScrollPane1,
                                                        javax.swing.GroupLayout.Alignment.LEADING,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                                        0,
                                                        Short.MAX_VALUE)
                                                .addComponent(jPanel4,
                                                        javax.swing.GroupLayout.Alignment.LEADING,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE))))
                        .addGap(50, 50, 50))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup().addGap(350, 350, 350)
                                .addComponent(jPanel1,
                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(350, Short.MAX_VALUE))));
        jPanel2Layout.setVerticalGroup(jPanel2Layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup().addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(
                                javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1,
                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                        40,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2,
                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                        40,
                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, 0)
                        .addGroup(jPanel2Layout.createParallelGroup(
                                javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane4,
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        300, Short.MAX_VALUE)
                                .addComponent(jScrollPane1))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(
                                javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jPanel4,
                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createParallelGroup(
                                        javax.swing.GroupLayout.Alignment.TRAILING,
                                        false)
                                        .addComponent(jButton2,
                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                0, Short.MAX_VALUE)
                                        .addComponent(jScrollPane2,
                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                38,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(28, 28, 28))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup().addGap(250, 250, 250)
                                .addComponent(jPanel1,
                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(250, Short.MAX_VALUE))));

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 800, 630);

        pack();
        setLocationRelativeTo(null);
    }

    private void jButton1ActionPerformed(final java.awt.event.ActionEvent evt)
            throws HeadlessException, IOException {
        final int t = JOptionPane.showConfirmDialog(new Chat(), "Do you really want to logout?", "Confirm",
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (t == 0) {
            try {
                String exitInfo = "*****" + Login.name + " has left the chat" + "*****";
                byte[] buffer = exitInfo.getBytes();
                DatagramPacket dataToSend = new DatagramPacket(buffer, buffer.length, Chat.GROUP1,
                        Chat.PORT1);
                Chat.dSocket.send(dataToSend);
            } catch (IOException ioe) {
            }
            Status.dSocket.close();
            ReceivedStatus.nameList.clear();
            try {
                DatagramPacket logoutStatus;
                String temp = "exited";
                byte[] buffer = temp.getBytes();
                logoutStatus = new DatagramPacket(buffer, 0, buffer.length, Chat.GROUP2, Chat.PORT2);
                dSocket.send(logoutStatus);
            } catch (IOException ioe) {
            }
            mSocket.leaveGroup(GROUP1);
            dSocket.close();
            this.setVisible(false);
            new Login().setVisible(true);
        }
    }

    private void jButton2ActionPerformed(final java.awt.event.ActionEvent evt) {
        if (jTextArea3.getText() != "") {
            try {
                String msg = Login.name + ": " + jTextArea3.getText();
                byte[] buffer = msg.getBytes();
                DatagramPacket dataToSend = new DatagramPacket(buffer, buffer.length, Chat.GROUP1,
                        Chat.PORT1);
                dSocket.send(dataToSend);
            } catch (IOException ioe) {
            }
        }
    }

    private void jButton2MouseReleased(final java.awt.event.MouseEvent evt) {
        try {
            jButton2.setIcon(
                    new javax.swing.ImageIcon((ImageIO.read(Login.class.getResourceAsStream("/img/send.png")))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void jButton2MousePressed(final java.awt.event.MouseEvent evt) {
        try {
            jButton2.setIcon(
                    new javax.swing.ImageIcon((ImageIO.read(Login.class.getResourceAsStream("/img/sent1.png")))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(final String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new Chat().setVisible(true);
            } catch (final IOException e) {
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    public static javax.swing.JTextArea jTextArea1;
    public static javax.swing.JTextArea jTextArea2;
    public static javax.swing.JTextArea jTextArea3;
    // End of variables declaration//GEN-END:variables
}

class Client implements Runnable {

    static Thread t3;

    Client() throws HeadlessException, IOException {
        try {
            Chat.mSocket = new MulticastSocket(Chat.PORT1);
            Chat.GROUP1 = InetAddress.getByName("230.0.0.1");
            Chat.mSocket.joinGroup(Chat.GROUP1);
            Chat.GROUP2 = InetAddress.getByName("230.0.0.2");
            Chat.dSocket = new DatagramSocket();

        } catch (IOException e) {
            JOptionPane.showMessageDialog(new Chat(), "Sorry,Cannot bind");
        }

    }

    public void run() {
        Thread t2 = new Thread(new ReceivedStatus());
        t2.start();

        t3 = new Thread(new Status());
        t3.start();

        newUser();
        while (true) {
            try {
                DatagramPacket dataToReceive;
                byte[] buffer = new byte[256];
                dataToReceive = new DatagramPacket(buffer, buffer.length);
                Chat.mSocket.receive(dataToReceive);
                String received = new String(dataToReceive.getData(), 0, dataToReceive.getLength());
                received = Chat.jTextArea1.getText() + received + "\n";
                Chat.jTextArea1.setText(received);
                Chat.jTextArea3.setText("");

            } catch (IOException e) {
                System.err.println(e);
            }
        }

    }

    void newUser() {
        try {
            String joinInfo = "***** " + Login.name + " has entered the chat" + " *****";
            byte[] buffer = joinInfo.getBytes();
            DatagramPacket dataToSend = new DatagramPacket(buffer, buffer.length, Chat.GROUP1, Chat.PORT1);
            Chat.dSocket.send(dataToSend);
        } catch (IOException ioe) {
            System.out.print("Can't set online status");
        }
    }
}

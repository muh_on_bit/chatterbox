package chatterbox;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.MulticastSocket;
import java.net.SocketException;

import java.util.ArrayList;

import static java.lang.Thread.sleep;

class ReceivedStatus implements Runnable {
    static ArrayList<String> nameList;
    MulticastSocket          mSocket;

    ReceivedStatus() {
        try {
            mSocket = new MulticastSocket(Chat.PORT2);
            mSocket.joinGroup(Chat.GROUP2);
        } catch (IOException e) {}
    }

    public void run() {
        nameList = new ArrayList<>();

        String temp;

        while (true) {
            try {
                byte[]         buffer      = new byte[256];
                DatagramPacket receiveData = new DatagramPacket(buffer, buffer.length);

                mSocket.receive(receiveData);
                temp = new String(receiveData.getData(), 0, receiveData.getLength());

                if (temp.equals("exited")) {
                    nameList.clear();
                }

                if (!nameList.contains(temp) &&!temp.equals("exited")) {
                    nameList.add(temp);
                    Chat.jTextArea2.setText("");

                    for (Object foo : nameList) {
                        Chat.jTextArea2.setText(Chat.jTextArea2.getText() + foo.toString() + "\n");
                    }
                }
            } catch (IOException e) {
                System.out.println("Couldn\'t receive status");
            }
        }
    }
}


public class Status implements Runnable {
    static DatagramSocket dSocket;

    Status() {
        try {
            dSocket = new DatagramSocket();
        } catch (SocketException ex) {}
    }

    public void run() {
        while (true) {
            byte[] buffer = new byte[256];

            buffer = Login.name.getBytes();

            DatagramPacket sendOnlineStatus = new DatagramPacket(buffer, buffer.length, Chat.GROUP2, Chat.PORT2);

            try {
                dSocket.send(sendOnlineStatus);
                sleep((long) (Math.random() * 10000));
            } catch (IOException e) {}
            catch (InterruptedException ex) {}
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
